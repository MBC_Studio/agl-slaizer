package ru.alsu.slaizer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.alsu.slaizer.dto.metric.MetricRequestDTO;
import ru.alsu.slaizer.dto.metric.MetricResponse;
import ru.alsu.slaizer.mapper.MetricMapper;
import ru.alsu.slaizer.model.Metric;
import ru.alsu.slaizer.service.MetricService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/projects/{projectId}/metrics")
@Tag(name = "Работа с метриками")
public class MetricController {
    private final MetricService service;
    private final MetricMapper mapper;

    @PostMapping
    @Operation(summary = "Создание метрики")
    public MetricResponse create(@RequestBody @Valid MetricRequestDTO metricRequestDTO,
                                 @PathVariable Long projectId) {
        Metric metric = mapper.toEntity(metricRequestDTO);
        return mapper.toResponse(service.create(metric, projectId));
    }

    @GetMapping("/{metricId}")
    @Operation(summary = "Получение метрики по id")
    public MetricResponse getById(@PathVariable Long metricId) {
        Metric metric = service.getById(metricId);
        return mapper.toResponse(metric);
    }

    @GetMapping
    @Operation(summary = "Получение метрик")
    public List<MetricResponse> getAllForProject(@PathVariable Long projectId) {
        List<Metric> metrics = service.getAllForProject(projectId);
        return mapper.toResponse(metrics);
    }

    @DeleteMapping("/{metricId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Удаление метрики")
    public void deleteById(@PathVariable Long metricId) {
        service.deleteById(metricId);
    }

    @PutMapping("/{metricId}")
    @Operation(summary = "Обновление метрики")
    public MetricResponse updateById(@PathVariable Long metricId,
                                     @RequestBody @Valid MetricRequestDTO request) {
        Metric metric = service.updateById(request, metricId);
        return mapper.toResponse(metric);
    }
}
