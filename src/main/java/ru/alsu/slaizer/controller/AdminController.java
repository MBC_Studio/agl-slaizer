package ru.alsu.slaizer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.alsu.slaizer.dto.admin.ProjectAdminResponse;
import ru.alsu.slaizer.dto.project.ProjectChangeStatusRequest;
import ru.alsu.slaizer.dto.user.UserChangeStatusRequest;
import ru.alsu.slaizer.dto.user.UserRequest;
import ru.alsu.slaizer.dto.user.UserResponse;
import ru.alsu.slaizer.mapper.ProjectMapper;
import ru.alsu.slaizer.mapper.UserMapper;
import ru.alsu.slaizer.service.ProjectService;
import ru.alsu.slaizer.service.UserService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/admins")
@Tag(name = "Действия администратора")
public class AdminController {
    private final ProjectService projectService;
    private final UserService userService;
    private final UserMapper userMapper;
    private final ProjectMapper projectMapper;

    @PatchMapping("/projects/{projectId}/status")
    @Operation(summary = "Изменение статуса проекта админом")
    public void changeProjectStatus(@PathVariable Long projectId,
                                    @RequestBody @Valid ProjectChangeStatusRequest request) {
        projectService.changeStatus(projectId, request.getStatus());
    }

    @GetMapping("/projects")
    @Operation(summary = "Получение всех проектов админом")
    public List<ProjectAdminResponse> getAllProjects() {
        return projectMapper.toAdminResponse(projectService.getAll());
    }

    @PatchMapping("/users/{userId}/status")
    @Operation(summary = "Изменение статуса пользователя админом")
    public void changeUserStatus(@PathVariable Long userId,
                                 @RequestBody @Valid UserChangeStatusRequest request) {
        userService.changeStatus(userId, request.getStatus());
    }

    @DeleteMapping("/users/{userId}")
    @Operation(summary = "Удаление пользователя админом")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUserById(@PathVariable Long userId) {
        userService.deleteById(userId);
    }

    @GetMapping("/users")
    @Operation(summary = "Получение всех пользователей админом")
    public List<UserResponse> getAllUsers() {
        return userMapper.toResponse(userService.getAll());
    }

    @PutMapping("/users/{userId}")
    @Operation(summary = "Редактирование пользователя админом")
    public UserResponse updateUser(@PathVariable Long userId, @RequestBody @Valid UserRequest request) {
        var user = userService.updateById(request, userId);
        return userMapper.toResponse(user);
    }

    @GetMapping("/users/{userId}")
    public UserResponse getUserById(@PathVariable Long userId) {
        var user = userService.getById(userId);
        return userMapper.toResponse(user);
    }
}
