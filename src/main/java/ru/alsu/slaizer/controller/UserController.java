package ru.alsu.slaizer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.alsu.slaizer.dto.user.UserChangePasswordRequest;
import ru.alsu.slaizer.service.UserService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
@Tag(name = "Работа с пользователями")
public class UserController {
    private final UserService service;

    @GetMapping("/check")
    @Operation(summary = "Проверка истечения сессии")
    public void checkAuth() {
    }

    @PutMapping("/change-password")
    public void changePassword(@RequestBody UserChangePasswordRequest request) {
        service.changePassword(request);
    }
}
