package ru.alsu.slaizer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.alsu.slaizer.dto.project.ProjectDashboardResponse;
import ru.alsu.slaizer.dto.project.ProjectRequest;
import ru.alsu.slaizer.dto.project.ProjectResponse;
import ru.alsu.slaizer.mapper.ProjectMapper;
import ru.alsu.slaizer.model.Project;
import ru.alsu.slaizer.service.DeleteService;
import ru.alsu.slaizer.service.ProjectDashboardService;
import ru.alsu.slaizer.service.ProjectService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/projects")
@Tag(name = "Работа с проектами")
public class ProjectController {
    private final ProjectService service;
    private final ProjectMapper mapper;
    private final ProjectDashboardService graphService;
    private final DeleteService deleteService;

    @PostMapping
    @Operation(summary = "Создание проекта")
    public ProjectResponse create(@RequestBody @Valid ProjectRequest projectRequest) {
        Project project = mapper.toEntity(projectRequest);
        return mapper.toResponse(service.create(project));
    }

    @GetMapping("/{projectId}")
    @Operation(summary = "Получение проекта по id")
    public ProjectResponse getById(@PathVariable Long projectId) {
        Project project = service.getById(projectId);
        return mapper.toResponse(project);
    }

    @GetMapping
    @Operation(summary = "Получение всех проектов")
    public List<ProjectResponse> getAll() {
        List<Project> projects = service.getAllByUser();
        return mapper.toResponse(projects);
    }

    @DeleteMapping("/{projectId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Удаление проекта")
    public void deleteById(@PathVariable Long projectId) {
        deleteService.deleteById(projectId);
    }

    @PutMapping("/{projectId}")
    @Operation(summary = "Обновление проекта")
    public ProjectResponse updateById(@PathVariable Long projectId, @RequestBody @Valid ProjectRequest request) {
        Project project = service.updateById(request, projectId);
        return mapper.toResponse(project);
    }

    @GetMapping("/{projectId}/dashboard")
    @Operation(summary = "Получение всей информации о проекте с графиками")
    public ProjectDashboardResponse getDashboardProject(@PathVariable Long projectId) {
        return graphService.getDashProject(projectId);
    }
}
