package ru.alsu.slaizer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.alsu.slaizer.dto.project.ProjectDashboardResponse;
import ru.alsu.slaizer.service.SharePageService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/share-pages")
@Tag(name = "Работа с публичными страницами для пользователей")
public class SharePageDashController {
    private final SharePageService sharePageService;

    @GetMapping("/{projectCode}")
    @Operation(summary = "Получение публичной страницы")
    public ProjectDashboardResponse getDashboardPage(@PathVariable String projectCode) {
        return sharePageService.getDashboardPage(projectCode);
    }
}
