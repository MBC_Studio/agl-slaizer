package ru.alsu.slaizer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.alsu.slaizer.dto.sharePage.SharePageChangeStatusRequest;
import ru.alsu.slaizer.dto.sharePage.SharePageRequest;
import ru.alsu.slaizer.dto.sharePage.SharePageResponse;
import ru.alsu.slaizer.mapper.SharePageMapper;
import ru.alsu.slaizer.model.SharePage;
import ru.alsu.slaizer.service.SharePageService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/projects/{projectId}/share-pages")
@Tag(name = "Работа с публичными страницами")
public class SharePageController {
    private final SharePageService service;
    private final SharePageMapper mapper;

    @GetMapping
    @Operation(summary = "Получение публичной страницы по id")
    public SharePageResponse getById(@PathVariable Long projectId) {
        SharePage sharePage = service.getByProjectId(projectId);
        return mapper.toResponse(sharePage);
    }

    @PutMapping("/{sharePageId}")
    @Operation(summary = "Обновление публичной страницы")
    public SharePageResponse updateById(@PathVariable Long sharePageId,
                                        @RequestBody @Valid SharePageRequest request) {
        SharePage sharePage = service.updateById(request, sharePageId);
        return mapper.toResponse(sharePage);
    }

    @PatchMapping("/{sharePageId}/status")
    @Operation(summary = "Изменение статуса публичной страницы")
    public void changeStatus(@PathVariable Long sharePageId,
                             @RequestBody @Valid SharePageChangeStatusRequest request) {
        service.changeStatus(sharePageId, request.getStatus());
    }

    @DeleteMapping("/{sharePageId}")
    @Operation(summary = "Удаление публичной страницы")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable Long sharePageId) {
        service.deleteById(sharePageId);
    }
}
