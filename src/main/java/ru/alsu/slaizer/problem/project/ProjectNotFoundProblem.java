package ru.alsu.slaizer.problem.project;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class ProjectNotFoundProblem extends AbstractThrowableProblem {
    public ProjectNotFoundProblem(Long value) {
        super(
                null,
                "Проект не найден",
                Status.NOT_FOUND,
                String.format("Проект с id %d не найден", value)
        );
    }
}
