package ru.alsu.slaizer.problem.project;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class ProjectCodeNotFoundProblem extends AbstractThrowableProblem {
    public ProjectCodeNotFoundProblem(String value) {
        super(
                null,
                "Проект не найден",
                Status.NOT_FOUND,
                String.format("Проект с кодом %s не найден", value)
        );
    }
}
