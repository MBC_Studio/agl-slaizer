package ru.alsu.slaizer.problem.user;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class UserNotUniqueUsernameProblem extends AbstractThrowableProblem {
    public UserNotUniqueUsernameProblem() {
        super(
                null,
                "Ник уже занят",
                Status.NOT_FOUND,
                "Пользователь с данным ником уже существует"
        );
    }
}
