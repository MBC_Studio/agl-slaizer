package ru.alsu.slaizer.problem.user;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class UserNotUniqueEmailProblem extends AbstractThrowableProblem {
    public UserNotUniqueEmailProblem() {
        super(
                null,
                "Email уже занят",
                Status.NOT_FOUND,
                "Пользователь с данным email уже существует"
        );
    }
}
