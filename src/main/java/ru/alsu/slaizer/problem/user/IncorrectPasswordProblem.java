package ru.alsu.slaizer.problem.user;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class IncorrectPasswordProblem extends AbstractThrowableProblem {

    public IncorrectPasswordProblem() {
        super(
                null,
                "Пароль неверный",
                Status.BAD_REQUEST,
                "Пароль пользователя неверный"
        );
    }
}
