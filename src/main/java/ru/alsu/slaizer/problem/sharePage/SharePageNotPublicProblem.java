package ru.alsu.slaizer.problem.sharePage;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class SharePageNotPublicProblem extends AbstractThrowableProblem {
    public SharePageNotPublicProblem() {
        super(
                null,
                "Публичная страница недоступна",
                Status.SERVICE_UNAVAILABLE,
                "В данный момент публичная страница недоступна"
        );
    }
}
