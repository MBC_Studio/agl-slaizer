package ru.alsu.slaizer.problem.sharePage;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class SharePageNotFoundProblem extends AbstractThrowableProblem {
    public SharePageNotFoundProblem(Long value) {
        super(
                null,
                "Публичная страница не найдена",
                Status.NOT_FOUND,
                String.format("Публичная страница с id %d не найдена", value)
        );
    }
}
