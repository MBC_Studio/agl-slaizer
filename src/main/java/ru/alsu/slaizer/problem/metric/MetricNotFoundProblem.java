package ru.alsu.slaizer.problem.metric;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class MetricNotFoundProblem extends AbstractThrowableProblem {
    public MetricNotFoundProblem(Long value) {
        super(
                null,
                "Метрика не найдена",
                Status.NOT_FOUND,
                String.format("Метрика с id %d не найдена", value)
        );
    }
}
