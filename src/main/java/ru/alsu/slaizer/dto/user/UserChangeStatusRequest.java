package ru.alsu.slaizer.dto.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alsu.slaizer.model.enums.UserStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(description = "Запрос на изменение статуса пользователя")
public class UserChangeStatusRequest {

    @Schema(description = "Статус пользователя", example = "ACTIVE")
    private UserStatus status;
}
