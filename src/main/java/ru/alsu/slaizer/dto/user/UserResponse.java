package ru.alsu.slaizer.dto.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alsu.slaizer.model.enums.Role;
import ru.alsu.slaizer.model.enums.UserStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Ответ на запрос пользователя")
public class UserResponse {

    @Schema(description = "ID пользователя", example = "1")
    private Long id;

    @Schema(description = "Имя пользователя", example = "Jon")
    private String username;

    @Schema(description = "Почта", example = "example@mail.ru")
    private String email;

    @Schema(description = "Статус", example = "ACTIVE")
    private UserStatus status;

    @Schema(description = "Роль", example = "ROLE_USER")
    private Role role;
}
