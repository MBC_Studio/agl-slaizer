package ru.alsu.slaizer.dto.user;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alsu.slaizer.model.enums.Role;
import ru.alsu.slaizer.model.enums.UserStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Запрос на редактирование пользователя админом")
public class UserRequest {

    @Schema(description = "Имя пользователя", example = "Jon")
    @Size(min = 4, max = 50, message = "Имя пользователя должно содержать от 4 до 50 символов")
    @NotBlank(message = "Имя пользователя не может быть пустыми")
    private String username;

    @Schema(description = "Почта", example = "example@mail.ru")
    @Size(min = 8, max = 255, message = "Длина почты должна быть от 8 до 255 символов")
    @NotBlank(message = "Почта не может быть пустой")
    @Email(message = "Введен неверный формат почты")
    private String email;

    @Schema(description = "Статус", example = "ACTIVE")
    private UserStatus status;

    @Schema(description = "Роль", example = "ROLE_USER")
    private Role role;
}
