package ru.alsu.slaizer.dto.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(description = "Запрос на изменение пароля")
public class UserChangePasswordRequest {

    @Schema(description = "Старый пароль", example = "password")
    private String oldPassword;

    @Schema(description = "Новый пароль", example = "newPassword")
    private String newPassword;
}
