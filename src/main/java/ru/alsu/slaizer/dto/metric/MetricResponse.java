package ru.alsu.slaizer.dto.metric;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alsu.slaizer.model.enums.GraphType;
import ru.alsu.slaizer.model.enums.MethodTypeStatus;
import ru.alsu.slaizer.model.enums.RequestInterval;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Ответ на запрос метрики")
public class MetricResponse {

    @Schema(description = "Идентификатор метрики", example = "1")
    private Long id;

    @Schema(description = "Название метрики", example = "Вход в аккаунт")
    private String name;

    @Schema(description = "Путь к запросу", example = "/about")
    private String path;

    @Schema(description = "Данные для запроса")
    private String data;

    @Schema(description = "Тип запроса", example = "GET")
    private MethodTypeStatus methodType;

    @Schema(description = "Интервал между запросами", example = "ONEMIN")
    private RequestInterval interval;

    @Schema(description = "Тип графика", example = "AVAILABILITY")
    private GraphType graphType;

    @Schema(description = "Показ на публичной странице", example = "true")
    private boolean showOnPage;
}
