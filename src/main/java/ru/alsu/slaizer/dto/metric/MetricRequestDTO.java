package ru.alsu.slaizer.dto.metric;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alsu.slaizer.model.enums.GraphType;
import ru.alsu.slaizer.model.enums.MethodTypeStatus;
import ru.alsu.slaizer.model.enums.RequestInterval;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(description = "Запрос на создание метрики")
public class MetricRequestDTO {

    @Schema(description = "Название метрики", example = "Вход в аккаунт")
    @Size(min = 3, max = 100, message = "Название должно быть от 3 до 100 символов")
    @NotBlank(message = "Название метрики не должно быть пустым")
    private String name;

    @Schema(description = "Путь к запросу", example = "/about")
    @Size(max = 255, message = "Путь к запросу должен быть до 255 символов")
    private String path;

    @Schema(description = "Данные для запроса")
    @Size(max = 1000, message = "Данные для запроса должны быть до 1000 символов")
    private String data;

    @Schema(description = "Тип запроса", example = "GET")
    private MethodTypeStatus methodType;

    @Schema(description = "Интервал между запросами", example = "ONEMIN")
    private RequestInterval interval;

    @Schema(description = "Тип графика", example = "AVAILABILITY")
    private GraphType graphType;

    @Schema(description = "Показ на публичной странице", example = "true")
    private Boolean showOnPage;
}
