package ru.alsu.slaizer.dto.project;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alsu.slaizer.dto.graph.GraphResponse;
import ru.alsu.slaizer.dto.sharePage.SharePageResponse;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Данные проекта для вывода на панель")
public class ProjectDashboardResponse extends ProjectResponse {

    @Schema(description = "Информация о публичной странице")
    private SharePageResponse sharePage;

    @Schema(description = "Графики")
    private List<GraphResponse> graphs;
}
