package ru.alsu.slaizer.dto.project;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Запрос на создание проекта")
public class ProjectRequest {
    @Schema(description = "Название проекта", example = "MBC Studio")
    @Size(min = 2, max = 255, message = "Название должно быть от 2 до 255 символов")
    @NotBlank(message = "Название не должно быть пустым")
    private String name;

    @Schema(description = "Код проекта", example = "BBW-2")
    @Size(min = 3, max = 255, message = "Код проекта должен быть от 3 до 255 символов")
    private String code;

    @Schema(description = "Домен проекта", example = "diary.mbc-studio.ru/")
    @Size(min = 3, max = 255, message = "Домен проекта должен иметь от 3 до 255 символов")
    @NotBlank(message = "Домен проекта не должен быть пустой")
    private String baseUrl;
}
