package ru.alsu.slaizer.dto.sharePage;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alsu.slaizer.model.enums.SharePageStatus;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Schema(description = "Запрос на изменение статуса публичной страницы")
public class SharePageChangeStatusRequest {

    @Schema(description = "Статус публичной страницы", example = "ACTIVE")
    private SharePageStatus status;
}
