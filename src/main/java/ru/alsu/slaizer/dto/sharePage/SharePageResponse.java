package ru.alsu.slaizer.dto.sharePage;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alsu.slaizer.model.enums.SharePageStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Ответ на запрос публичной страницы")
public class SharePageResponse {

    @Schema(description = "Идентификатор публичной страницы", example = "1")
    private Long id;

    @Schema(description = "Название публичной страницы", example = "MBC Studio metrics")
    private String title;

    @Schema(description = "Описание публичной страницы", example = "Здесь вы найдете все необходимые метрики для проверки доступности сайта")
    private String description;

    @Schema(description = "Количество посетителей публичной страницы", example = "100")
    private Integer visits;

    @Schema(description = "Статус публичной страницы", example = "ACTIVE")
    private SharePageStatus status;
}
