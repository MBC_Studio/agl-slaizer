package ru.alsu.slaizer.dto.sharePage;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(description = "Запрос на создание публичной страницы")
public class SharePageRequest {

    @Schema(description = "Название публичной страницы", example = "MBC Studio metrics")
    @Size(min = 3, max = 60, message = "Название должно быть от 3 до 60 символов")
    @NotBlank(message = "Название не должно быть пустым")
    private String title;

    @Schema(description = "Описание публичной страницы", example = "Здесь вы найдете все необходимые метрики для проверки доступности сайта")
    @Size(min = 5, max = 1000, message = "Описание должно быть от 5 до 1000 символов")
    private String description;
}
