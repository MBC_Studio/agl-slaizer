package ru.alsu.slaizer.dto.graph;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import ru.alsu.slaizer.model.enums.GraphType;

import java.util.List;


@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Информация о графике")
public class GraphResponse {

    @Schema(description = "Имя графика", example = "Доступность главной страницы")
    private String name;

    @Schema(description = "Точки на графике")
    private GraphType graphType;

    @Schema(description = "Точки на графике времени отклика")
    private List<GraphUnit> responseTimeUnits;

    @Schema(description = "Точки на графике доступности")
    private List<GraphUnit> availabilityUnits;
}
