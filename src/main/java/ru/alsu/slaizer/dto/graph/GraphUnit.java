package ru.alsu.slaizer.dto.graph;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Точка на графика")
public class GraphUnit {

    @Schema(description = "Дата запроса")
    private OffsetDateTime timestamp;

    @Schema(description = "Время ответа", example = "150")
    private Long value;
}
