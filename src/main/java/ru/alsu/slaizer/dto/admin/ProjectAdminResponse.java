package ru.alsu.slaizer.dto.admin;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alsu.slaizer.dto.user.UserResponse;
import ru.alsu.slaizer.model.enums.ProjectStatus;

import java.time.OffsetDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Ответ на запрос проекта администратором")
public class ProjectAdminResponse {
    @Schema(description = "Идентификатор проекта", example = "1")
    private Long id;

    @Schema(description = "Название проекта", example = "MBS Studio")
    private String name;

    @Schema(description = "Код проекта", example = "BBW-2")
    private String code;

    @Schema(description = "Ссылка на проект", example = "https://diary.mbc-studio.ru/")
    private String baseUrl;

    @Schema(description = "Статус проекта", example = "ACTIVE")
    private ProjectStatus status;

    @Schema(description = "Время окончания отправки запросов", example = "2025-10-10T10:10:10.000+00:00")
    private OffsetDateTime endTime;

    @Schema(description = "Пользователь проекта")
    private UserResponse user;
}
