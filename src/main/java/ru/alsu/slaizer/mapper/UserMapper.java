package ru.alsu.slaizer.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import ru.alsu.slaizer.dto.user.UserRequest;
import ru.alsu.slaizer.dto.user.UserResponse;
import ru.alsu.slaizer.model.User;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface UserMapper {
    User toEntity(UserRequest userRequest);

    UserResponse toResponse(User user);

    List<UserResponse> toResponse(List<User> users);
}
