package ru.alsu.slaizer.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import ru.alsu.slaizer.dto.admin.ProjectAdminResponse;
import ru.alsu.slaizer.dto.project.ProjectRequest;
import ru.alsu.slaizer.dto.project.ProjectResponse;
import ru.alsu.slaizer.model.Project;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = {UserMapper.class})
public interface ProjectMapper {
    ProjectResponse toResponse(Project project);

    Project toEntity(ProjectRequest projectRequest);

    List<ProjectResponse> toResponse(List<Project> projects);

    ProjectAdminResponse toAdminResponse(Project project);

    List<ProjectAdminResponse> toAdminResponse(List<Project> projects);
}
