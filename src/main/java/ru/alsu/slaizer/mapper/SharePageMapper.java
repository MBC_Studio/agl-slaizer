package ru.alsu.slaizer.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import ru.alsu.slaizer.dto.sharePage.SharePageRequest;
import ru.alsu.slaizer.dto.sharePage.SharePageResponse;
import ru.alsu.slaizer.model.SharePage;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SharePageMapper {
    SharePage toEntity(SharePageRequest request);

    SharePageResponse toResponse(SharePage sharePage);
}
