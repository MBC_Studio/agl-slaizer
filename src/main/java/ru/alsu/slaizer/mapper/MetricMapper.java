package ru.alsu.slaizer.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import ru.alsu.slaizer.dto.metric.MetricRequestDTO;
import ru.alsu.slaizer.dto.metric.MetricResponse;
import ru.alsu.slaizer.model.Metric;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MetricMapper {
    Metric toEntity(MetricRequestDTO metricRequestDTO);

    MetricResponse toResponse(Metric metric);

    List<MetricResponse> toResponse(List<Metric> metrics);
}
