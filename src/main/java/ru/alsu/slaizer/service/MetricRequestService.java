package ru.alsu.slaizer.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.alsu.slaizer.dto.graph.GraphUnit;
import ru.alsu.slaizer.model.Metric;
import ru.alsu.slaizer.model.MetricRequest;
import ru.alsu.slaizer.repository.MetricRequestRepository;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MetricRequestService {
    private final MetricRequestRepository repository;
    public static final int ONE_DAY = 24;
    public static final int NINETY_DAYS = 90;

    public MetricRequest save(MetricRequest metricRequest) {
        return repository.save(metricRequest);
    }

    /**
     * Метод возвращает все точки на графике времени ответа, взяв данные для определенной метрики из metricRequest
     *
     * @param metric
     * @return список точек
     */
    public List<GraphUnit> getResponseTimeUnits(Metric metric) {
        var metricRequests = repository.findAllByMetricAndCreatedAtAfterOrderByCreatedAt(metric,
                OffsetDateTime
                        .now()
                        .minusHours(ONE_DAY));
        return metricRequests.stream().map(this::getGraphUnit).toList();
    }

    /**
     * Метод возвращает все точки на графике доступности, высчитывая процент доступности для каждого дня
     *
     * @param metric
     * @return список точек
     */
    public List<GraphUnit> getAvailabilityUnits(Metric metric) {

        var date = OffsetDateTime
                .now()
                .withHour(0).withMinute(0).withSecond(0).withNano(0)
                .minusDays(NINETY_DAYS - 1);
        var metricRequests = repository.findAllByMetricAndCreatedAtAfterOrderByCreatedAt(metric, date);

        List<GraphUnit> graphUnits = new ArrayList<>();

        var cm = 0;

        for (int i = 0; i < 90; i++) {
            double successPercent = -1;
            List<MetricRequest> requests = new ArrayList<>();

            while (metricRequests.size() > cm && metricRequests.get(cm).getCreatedAt().isBefore(date.plusDays(1))) {
                requests.add(metricRequests.get(cm));
                cm++;
            }
            if (!requests.isEmpty()) {
                double successRequests = 0;
                double badRequests = 0;

                for (var request : requests) {
                    if (String.valueOf(request.getStatus()).startsWith("2")) {
                        successRequests++;
                    } else {
                        badRequests++;
                    }
                }
                successPercent = (successRequests / (successRequests + badRequests)) * 100;
            }
            GraphUnit graphUnit = new GraphUnit(date, (long) successPercent);
            graphUnits.add(graphUnit);
           date = date.plusDays(1);
        }
        return graphUnits;
    }

    /**
     * Маппер для передачи данных в график
     *
     * @param metricRequest
     * @return graphUnit
     */
    private GraphUnit getGraphUnit(MetricRequest metricRequest) {
        var graphUnit = new GraphUnit();
        graphUnit.setTimestamp(metricRequest.getCreatedAt());
        graphUnit.setValue(metricRequest.getResponseTime());

        return graphUnit;
    }

    public void deleteAllByMetric(Metric metric) {
        repository.deleteAllByMetric(metric);
    }
}
