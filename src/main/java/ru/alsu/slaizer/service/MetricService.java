package ru.alsu.slaizer.service;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.alsu.slaizer.dto.graph.GraphResponse;
import ru.alsu.slaizer.dto.metric.MetricRequestDTO;
import ru.alsu.slaizer.model.Metric;
import ru.alsu.slaizer.model.MetricRequest;
import ru.alsu.slaizer.model.Project;
import ru.alsu.slaizer.model.enums.GraphType;
import ru.alsu.slaizer.model.enums.ProjectStatus;
import ru.alsu.slaizer.problem.metric.MetricNotFoundProblem;
import ru.alsu.slaizer.repository.MetricRepository;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
public class MetricService {
    private final MetricRepository repository;
    private final ProjectService projectService;
    private final UserService userService;
    private final MetricRequestService metricRequestService;

    public Metric save(Metric metric) {
        return repository.save(metric);
    }

    public List<Metric> saveAll(List<Metric> metrics) {
        return repository.saveAll(metrics);
    }

    public Metric create(Metric metric, Long projectId) {
        var project = projectService.getById(projectId);
        metric.setProject(project);

        var sendTime = getFirstRequest(metric);
        metric.setNextRequest(sendTime);
        return save(metric);
    }

    /**
     * Метод для получения времени первого запроса для метрики, для "ровного" графика
     *
     * @param metric
     * @return время первого запроса
     */
    private static OffsetDateTime getFirstRequest(Metric metric) {
        var currentTime = OffsetDateTime.now();
        var sendTime = currentTime.withHour(0).withMinute(0).withSecond(0).withNano(0);
        while (sendTime.isBefore(currentTime)) {
            sendTime = sendTime.plusMinutes(metric.getInterval().getAmountMin());
        }
        return sendTime;
    }

    /**
     * Метод для обновления всех первых запросов во всех активных метриках при обновлении приложения
     */
    @PostConstruct
    @Transactional
    public void initializeMetrics() {
        var activeMetricsInProject = repository.findAllByProjectStatus(ProjectStatus.ACTIVE);
        for (Metric metric : activeMetricsInProject) {
            var firstRequestTime = getFirstRequest(metric);
            metric.setNextRequest(firstRequestTime);
            save(metric);
        }
    }

    public Metric getById(Long metricId) {
        var user = userService.getCurrentUser();
        var metric = repository.findById(metricId)
                .orElseThrow(() -> new MetricNotFoundProblem(metricId));
        var userMetric = metric.getProject().getUser();
        if (Objects.equals(user.getId(), userMetric.getId()) || user.isAdmin()) {
            return metric;
        } else {
            throw new MetricNotFoundProblem(metricId);
        }
    }

    public List<Metric> getAllForProject(Long projectId) {
        var project = projectService.getById(projectId);
        return project.getMetrics();
    }

    /**
     * Метод вывода всех активных метрик конкретного проекта
     * @param projectId
     * @return активные метрики
     */
    public List<Metric> getAllOnPage(Long projectId) {
        var project = projectService.getById(projectId);
        return repository.findAllByProjectAndShowOnPage(project, true);
    }

    @Transactional
    public void deleteById(Long metricId) {
        var metric = getById(metricId);
        metricRequestService.deleteAllByMetric(metric);
        repository.deleteById(metric.getId());
    }

    public Metric updateById(MetricRequestDTO request, Long metricId) {
        var metric = getById(metricId);
        metric.setName(request.getName());
        metric.setPath(request.getPath());
        metric.setData(request.getData());
        metric.setGraphType(request.getGraphType());
        metric.setInterval(request.getInterval());
        metric.setMethodType(request.getMethodType());
        metric.setShowOnPage(request.getShowOnPage());
        return save(metric);
    }

    /**
     * Метод отправки запроса и вычисления его времени ответа, статуса и т.д.
     * @param metric
     */
    @Async
    public void sendRequest(Metric metric) {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(metric.getFullUrl());

            long startTime = System.currentTimeMillis();
            try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                long executionTime = System.currentTimeMillis() - startTime;

                var result = MetricRequest.builder()
                        .metric(metric)
                        .status(response.getCode())
                        .responseTime(executionTime)
                        .createdAt(metric.getNextRequest())
                        .build();
                metricRequestService.save(result);
                log.info("Request to {} completed in {} ms", metric.getFullUrl(), executionTime);
            }
        } catch (IOException e) {
            var result = MetricRequest.builder()
                    .metric(metric)
                    .status(-1)
                    .createdAt(metric.getNextRequest())
                    .build();
            metricRequestService.save(result);
            log.error("Request to {} failed with error: {}", metric.getFullUrl(), e.toString());
        }
    }

    /**
     * Метод нахождения всех метрик, у которых время следующего запроса меньше текущего
     *
     * @return список метрик
     */
    public List<Metric> getAllForSendRequest() {
        var currentTime = OffsetDateTime.now();
        return repository.findAllByNextRequestLessThanEqual(currentTime);
    }

    /**
     * Метод получения метрик для графиков
     *
     * @param metrics метрики
     * @return список графиков
     */
    public List<GraphResponse> getGraphs(List<Metric> metrics) {
        List<GraphResponse> graphs = new ArrayList<>();
        for (var metric : metrics) {
            var graphResponse = new GraphResponse();
            graphResponse.setName(metric.getName());
            graphResponse.setGraphType(metric.getGraphType());
            if (metric.getGraphType() == GraphType.AVAILABILITY) {
                graphResponse.setAvailabilityUnits(metricRequestService.getAvailabilityUnits(metric));
            } else if (metric.getGraphType() == GraphType.RESPONSETIME) {
                graphResponse.setResponseTimeUnits(metricRequestService.getResponseTimeUnits(metric));
            } else if (metric.getGraphType() == GraphType.COMBINED) {
                graphResponse.setAvailabilityUnits(metricRequestService.getAvailabilityUnits(metric));
                graphResponse.setResponseTimeUnits(metricRequestService.getResponseTimeUnits(metric));
            }
            graphs.add(graphResponse);
        }
        return graphs;
    }

    public void deleteAllByProject(Project project) {
        repository.deleteAllByProject(project);
    }
}