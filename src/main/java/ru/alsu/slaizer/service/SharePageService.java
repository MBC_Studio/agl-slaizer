package ru.alsu.slaizer.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.alsu.slaizer.dto.project.ProjectDashboardResponse;
import ru.alsu.slaizer.dto.sharePage.SharePageRequest;
import ru.alsu.slaizer.mapper.SharePageMapper;
import ru.alsu.slaizer.model.SharePage;
import ru.alsu.slaizer.model.enums.ProjectStatus;
import ru.alsu.slaizer.model.enums.SharePageStatus;
import ru.alsu.slaizer.problem.sharePage.SharePageNotFoundProblem;
import ru.alsu.slaizer.problem.sharePage.SharePageNotPublicProblem;
import ru.alsu.slaizer.repository.SharePageRepository;

import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SharePageService {
    private final SharePageRepository repository;
    private final ProjectService projectService;
    private final UserService userService;
    private final MetricService metricService;
    private final SharePageMapper mapper;

    public SharePage save(SharePage sharePage) {
        return repository.save(sharePage);
    }

    public static SharePage create(String projectName) {
        var sharePage = new SharePage();
        sharePage.setStatus(SharePageStatus.INACTIVE);
        sharePage.setVisits(0);
        sharePage.setTitle("Статистика проекта " + projectName);
        sharePage.setDescription("Добро пожаловать на страницу " + projectName +
                "! Здесь вы можете отслеживать различные показатели веб-сайта или приложения.");
        return sharePage;
    }

    public Optional<SharePage> findById(Long sharePageId) {
        return repository.findById(sharePageId);
    }

    public SharePage getByProjectId(Long projectId) {
        return projectService.getById(projectId).getSharePage();
    }

    public SharePage getById(Long sharePageId) {
        var user = userService.getCurrentUser();
        var sharePage = findById(sharePageId)
                .orElseThrow(() -> new SharePageNotFoundProblem(sharePageId));
        var userSharePage = sharePage.getProject().getUser();
        if (Objects.equals(user.getId(), userSharePage.getId()) || user.isAdmin()) {
            return sharePage;
        } else {
            throw new SharePageNotFoundProblem(sharePageId);
        }
    }

    public SharePage updateById(SharePageRequest request, Long sharePageId) {
        var sharePage = getById(sharePageId);
        sharePage.setTitle(request.getTitle());
        sharePage.setDescription(request.getDescription());
        return save(sharePage);
    }

    public void changeStatus(Long sharePageId, SharePageStatus status) {
        var sharePage = getById(sharePageId);
        sharePage.setStatus(status);
        save(sharePage);
    }

    public void deleteById(Long sharePageId) {
        var sharePage = getById(sharePageId);
        repository.deleteById(sharePage.getId());
    }

    /**
     * Метод для выдачи публичной страницы, вычисления посетителей и обновления счётчика
     *
     * @param code
     */
    public ProjectDashboardResponse getDashboardPage(String code) {
        var project = projectService.findByCode(code);
        var sharePage = project.getSharePage();
        if (sharePage.getStatus() == SharePageStatus.INACTIVE || project.getStatus() == ProjectStatus.BLOCKED) {
            throw new SharePageNotPublicProblem();
        }

        sharePage.setVisits(sharePage.getVisits() + 1);
        save(sharePage);

        var response = new ProjectDashboardResponse();
        response.setId(project.getId());
        response.setName(project.getName());
        response.setCode(project.getCode());
        response.setBaseUrl(project.getBaseUrl());
        response.setStatus(project.getStatus());
        response.setEndTime(project.getEndTime());

        response.setSharePage(mapper.toResponse(project.getSharePage()));
        response.setGraphs(metricService.getGraphs(metricService.getAllOnPage(project.getId())));

        return response;
    }
}
