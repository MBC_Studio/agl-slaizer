package ru.alsu.slaizer.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.alsu.slaizer.dto.project.ProjectDashboardResponse;
import ru.alsu.slaizer.mapper.SharePageMapper;
import ru.alsu.slaizer.model.enums.ProjectStatus;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProjectDashboardService {
    private final ProjectService projectService;
    private final MetricService metricService;
    private final SharePageMapper sharePageMapper;

    /**
     * Метод вывода всей информации о проекте - имени, графиках и т.д.
     *
     * @param projectId
     * @return projectDashboardResponse
     */
    public ProjectDashboardResponse getDashProject(Long projectId) {
        var project = projectService.getById(projectId);
        if (project.getStatus() == ProjectStatus.ACTIVE) {
            var response = new ProjectDashboardResponse();
            response.setId(project.getId());
            response.setName(project.getName());
            response.setCode(project.getCode());
            response.setBaseUrl(project.getBaseUrl());
            response.setStatus(project.getStatus());
            response.setEndTime(project.getEndTime());

            response.setSharePage(sharePageMapper.toResponse(project.getSharePage()));
            response.setGraphs(metricService.getGraphs(project.getMetrics()));

            return response;
        }
        return null;
    }
}
