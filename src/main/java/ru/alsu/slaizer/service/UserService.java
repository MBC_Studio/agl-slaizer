package ru.alsu.slaizer.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.alsu.slaizer.dto.user.UserChangePasswordRequest;
import ru.alsu.slaizer.dto.user.UserRequest;
import ru.alsu.slaizer.model.User;
import ru.alsu.slaizer.model.enums.UserStatus;
import ru.alsu.slaizer.problem.user.IncorrectPasswordProblem;
import ru.alsu.slaizer.problem.user.UserNotUniqueEmailProblem;
import ru.alsu.slaizer.problem.user.UserNotUniqueUsernameProblem;
import ru.alsu.slaizer.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    /**
     * Сохранение пользователя
     *
     * @return сохраненный пользователь
     */
    public User save(User user) {
        return repository.save(user);
    }


    /**
     * Создание пользователя
     *
     * @return созданный пользователь
     */
    public User create(User user) {
        if (repository.existsByUsername(user.getUsername())) {
            throw new UserNotUniqueUsernameProblem();
        }

        if (repository.existsByEmail(user.getEmail())) {
            throw new UserNotUniqueEmailProblem();
        }

        user.setStatus(UserStatus.ACTIVE);
        //TODO: сделать у пользователя статус WAITING перед подтверждением почты

        return save(user);
    }

    /**
     * Получение пользователя по имени пользователя
     *
     * @return пользователь
     */
    public User getByUsername(String username) {
        return repository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Пользователь не найден"));
    }

    /**
     * Получение пользователя по имени пользователя
     * <p>
     * Нужен для Spring Security
     *
     * @return пользователь
     */
    public UserDetailsService userDetailsService() {
        return this::getByUsername;
    }

    /**
     * Получение текущего пользователя
     *
     * @return текущий пользователь
     */
    public User getCurrentUser() {
        // Получение имени пользователя из контекста Spring Security
        var username = SecurityContextHolder.getContext().getAuthentication().getName();
        return getByUsername(username);
    }

    public User getById(Long userId) {
        return findById(userId)
                .orElseThrow(() -> new UsernameNotFoundException("Пользователь с данным id не найден"));
    }

    public Optional<User> findById(Long userId) {
        return repository.findById(userId);
    }

    public void changeStatus(Long userId, UserStatus status) {
        var user = getById(userId);
        user.setStatus(status);
        save(user);
    }

    public void deleteById(Long userId) {
        var user = getById(userId);
        repository.deleteById(user.getId());
    }

    public List<User> getAll() {
        if (getCurrentUser().isAdmin()){
            return repository.findAll();
        }
        return null;
    }

    /**
     * Создание пользователя админом
     *
     * @return созданный пользователь
     */
    public User createWithRole(User user) {
        if (repository.existsByUsername(user.getUsername())) {
            throw new UserNotUniqueUsernameProblem();
        }

        if (repository.existsByEmail(user.getEmail())) {
            throw new UserNotUniqueEmailProblem();
        }

        user.setStatus(UserStatus.ACTIVE);
        //TODO: сделать у пользователя статус WAITING перед подтверждением почты

        return save(user);
    }

    /**
     * Изменение пароля
     *
     * @param request запрос на изменение пароля
     */
    public void changePassword(UserChangePasswordRequest request) {
        User user = getCurrentUser();

        // Проверяем, соответствует ли текущий пароль паролю пользователя
        if (!passwordEncoder.matches(request.getOldPassword(), user.getPassword())) {
            throw new IncorrectPasswordProblem();
        }

        setNewPassword(user, request.getNewPassword());
    }

    /**
     * Установка нового пароля для пользователя
     * @param user     пользователь
     * @param password новый пароль
     */
    public void setNewPassword(User user, String password) {
        // Хэшируем и устанавливаем новый пароль
        String encodedPassword = passwordEncoder.encode(password);
        user.setPassword(encodedPassword);

        // Сохраняем пользователя с обновленным паролем
        repository.save(user);
    }

    /**
     * Редактирование пользователя админом
     * @param request данные о пользователе
     * @param userId id пользователя
     * @return пользователь с новыми данными
     */
    public User updateById(UserRequest request, Long userId) {
        if (getCurrentUser().isAdmin()) {
            var user = getById(userId);
            user.setUsername(request.getUsername());
            user.setEmail(request.getEmail());
            user.setStatus(request.getStatus());
            user.setRole(request.getRole());
            return save(user);
        }
        return null;
    }
}