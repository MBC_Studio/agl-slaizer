package ru.alsu.slaizer.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Service
@Slf4j
@RequiredArgsConstructor
public class SchedulerService {
    private final MetricService metricService;

    /**
     * Метод отправки запросов и обновления следующего времени запроса
     */
    @Scheduled(cron = "0 * * * * *")
    @Async
    public void processMetrics() {
        var metrics = metricService.getAllForSendRequest();

        metrics.forEach(metric -> {
            metricService.sendRequest(metric);

            OffsetDateTime nextRequest = metric.getNextRequest().plusMinutes(metric.getInterval().getAmountMin());
            metric.setNextRequest(nextRequest);
        });

        metricService.saveAll(metrics);
    }
}
