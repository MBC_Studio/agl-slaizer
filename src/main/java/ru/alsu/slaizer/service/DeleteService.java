package ru.alsu.slaizer.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.alsu.slaizer.repository.MetricRequestRepository;
import ru.alsu.slaizer.repository.ProjectRepository;

@Service
@RequiredArgsConstructor
public class DeleteService {
    private final ProjectService projectService;
    private final MetricService metricService;
    private final ProjectRepository projectRepository;
    private final MetricRequestRepository metricRequestRepository;

    @Transactional
    public void deleteById(Long projectId) {
        var project = projectService.getById(projectId);
        metricRequestRepository.deleteAllByMetricProject(project);
        metricService.deleteAllByProject(project);
        projectRepository.deleteById(project.getId());
    }
}
