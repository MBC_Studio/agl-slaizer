package ru.alsu.slaizer.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.alsu.slaizer.dto.project.ProjectRequest;
import ru.alsu.slaizer.model.Project;
import ru.alsu.slaizer.model.enums.ProjectStatus;
import ru.alsu.slaizer.problem.project.ProjectCodeNotFoundProblem;
import ru.alsu.slaizer.problem.project.ProjectNotFoundProblem;
import ru.alsu.slaizer.repository.ProjectRepository;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProjectService {
    private final ProjectRepository repository;
    private final UserService userService;

    public Project save(Project project) {
        return repository.save(project);
    }

    public Project create(Project project) {
        project.setUser(userService.getCurrentUser());
        project.setStatus(ProjectStatus.ACTIVE);
        project.setEndTime(OffsetDateTime.now().plusYears(1));
        var sharePage = SharePageService.create(project.getName());
        sharePage.setProject(project);
        project.setSharePage(sharePage);
        return save(project);
    }

    public Optional<Project> findById(Long projectId) {
        return repository.findById(projectId);
    }

    public Project findByCode(String code) {
        return repository.findByCode(code).orElseThrow(() -> new ProjectCodeNotFoundProblem(code));
    }

    public Project getById(Long projectId) {
        var project = findById(projectId)
                .orElseThrow(() -> new ProjectNotFoundProblem(projectId));
        var user = userService.getCurrentUser();
        var userProject = project.getUser();
        if (Objects.equals(user.getId(), userProject.getId()) || user.isAdmin()) {
            return project;
        } else {
            throw new ProjectNotFoundProblem(projectId);
        }
    }

    public List<Project> getAllByUser() {
        var user = userService.getCurrentUser();
        return user.getProjects();
    }

    public List<Project> getAll() {
        return repository.findAll();
    }

    public Project updateById(ProjectRequest request, Long projectId) {
        var project = getById(projectId);
        project.setName(request.getName());
        project.setCode(request.getCode());
        project.setBaseUrl(request.getBaseUrl());
        return save(project);
    }

    public void changeStatus(Long projectId, ProjectStatus status) {
        var project = getById(projectId);
        project.setStatus(status);
        save(project);
    }
}
