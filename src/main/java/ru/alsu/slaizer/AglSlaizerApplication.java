package ru.alsu.slaizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AglSlaizerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AglSlaizerApplication.class, args);
    }

}
