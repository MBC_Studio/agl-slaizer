package ru.alsu.slaizer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.alsu.slaizer.model.SharePage;

public interface SharePageRepository extends JpaRepository<SharePage, Long> {
}
