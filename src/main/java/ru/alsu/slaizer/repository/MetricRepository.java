package ru.alsu.slaizer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.alsu.slaizer.model.Metric;
import ru.alsu.slaizer.model.Project;
import ru.alsu.slaizer.model.enums.ProjectStatus;

import java.time.OffsetDateTime;
import java.util.List;

public interface MetricRepository extends JpaRepository<Metric, Long> {
    List<Metric> findAllByNextRequestLessThanEqual(OffsetDateTime time);

    List<Metric> findAllByProjectStatus(ProjectStatus projectStatus);

    List<Metric> findAllByProjectAndShowOnPage(Project project, boolean showOnPage);

    void deleteAllByProject(Project project);
}
