package ru.alsu.slaizer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.alsu.slaizer.model.Project;

import java.util.Optional;

public interface ProjectRepository extends JpaRepository<Project, Long> {
    Optional<Project> findByCode(String code);
}
