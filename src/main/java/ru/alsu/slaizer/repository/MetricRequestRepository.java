package ru.alsu.slaizer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.alsu.slaizer.model.Metric;
import ru.alsu.slaizer.model.MetricRequest;
import ru.alsu.slaizer.model.Project;

import java.time.OffsetDateTime;
import java.util.List;

public interface MetricRequestRepository extends JpaRepository<MetricRequest, Long> {

    /**
     * Вывод всех ответов на запросы конкретной метрики за последние n часов
     * @param metric метрика
     * @param timestamp время, за которое необходимо вернуть все запросы
     * @return список ответов на запросы
     */
    List<MetricRequest> findAllByMetricAndCreatedAtAfterOrderByCreatedAt(Metric metric, OffsetDateTime timestamp);

    /**
     * Удаление всех ответов на запрос к сайту, перед удалением метрики
     * @param metric метрика
     */
    void deleteAllByMetric(Metric metric);

    void deleteAllByMetricProject(Project project);
}
