package ru.alsu.slaizer.model.enums;

public enum ProjectStatus {
    ACTIVE,
    BLOCKED,
}
