package ru.alsu.slaizer.model.enums;

public enum UserStatus {
    ACTIVE,
    BLOCKED,
    WAITING
}
