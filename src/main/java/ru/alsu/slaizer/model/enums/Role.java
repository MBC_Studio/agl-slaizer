package ru.alsu.slaizer.model.enums;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}
