package ru.alsu.slaizer.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum RequestInterval {
    ONEMIN("1 минута", 1),
    FIVEMIN("5 минут", 5),
    FIFTEENMIN("15 минут", 15),
    THIRTYMIN("30 минут", 30),
    ONEHOUR(" 1 час", 60),
    THREEHOURS("3 часа", 180),
    FIVEHOURS("5 часов", 300),
    TWELVEHOURS("12 часов", 720),
    ONEDAY("1 день", 1440);

    private final String title;
    private final Integer amountMin;
}
