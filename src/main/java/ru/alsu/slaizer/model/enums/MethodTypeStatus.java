package ru.alsu.slaizer.model.enums;

public enum MethodTypeStatus {
    GET,
    POST,
    PUT,
    DELETE,
    PATCH
}
