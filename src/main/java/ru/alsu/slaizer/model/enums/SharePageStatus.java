package ru.alsu.slaizer.model.enums;

public enum SharePageStatus {
    ACTIVE,
    INACTIVE
}
