package ru.alsu.slaizer.model.enums;

public enum GraphType {
    AVAILABILITY,
    RESPONSETIME,
    COMBINED
}
