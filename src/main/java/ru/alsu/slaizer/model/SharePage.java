package ru.alsu.slaizer.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alsu.slaizer.model.enums.SharePageStatus;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "share_page")
public class SharePage {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "share_page_id_seq")
    @SequenceGenerator(name = "share_page_id_seq", sequenceName = "share_page_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "title", length = 1000)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "visits")
    private Integer visits;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private SharePageStatus status;

    @OneToOne
    @JoinColumn(name = "project_id")
    private Project project;
}
