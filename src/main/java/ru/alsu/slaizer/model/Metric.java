package ru.alsu.slaizer.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alsu.slaizer.model.enums.GraphType;
import ru.alsu.slaizer.model.enums.MethodTypeStatus;
import ru.alsu.slaizer.model.enums.RequestInterval;

import java.time.OffsetDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "metric")
public class Metric {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "metric_id_seq")
    @SequenceGenerator(name = "metric_id_seq", sequenceName = "metric_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "path")
    private String path;

    @Column(name = "data")
    private String data;

    @Column(name = "method_type")
    @Enumerated(EnumType.STRING)
    private MethodTypeStatus methodType;

    @Column(name = "interval")
    @Enumerated(EnumType.STRING)
    private RequestInterval interval;

    @Column(name = "graph_type")
    @Enumerated(EnumType.STRING)
    private GraphType graphType;

    @Column(name = "next_request")
    private OffsetDateTime nextRequest;

    @OneToMany(mappedBy = "metric")
    private List<MetricRequest> requests;

    @Column(name = "show_on_page")
    private Boolean showOnPage;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    public String getFullUrl() {
        return project.getBaseUrl() + "/" + getPath();
    }
}
